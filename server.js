const express = require('express');

const port = 4000;
const app = express();

app.get('/', (req, res) => {
  res.send('Testing PM2, Adding another line');
});

app.listen(port, () => console.log(`PM2 running on ${port}`));
